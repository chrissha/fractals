import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import ArtistAnimation
from cmath import exp, pi
from multiprocessing import Pool
from progress.bar import Bar
from itertools import repeat

fig = plt.figure()  # For some reason this needs to be at the top

def compute_julia(frame, p, z, color, max_iter=25):
    c = 0.7885 * exp(1j * p)
    
    for i in range(max_iter):
        color[z.real**2 + z.imag**2 < 4] = i
        z = np.where(z.real**2 + z.imag**2 < 4, z**2 + c, z)    
    
    return color

def create_artist_list(ims):
    # Creates a list of artist objects out of a numpy array
    artist_list = []
    print(ims.shape)
    for im in ims:
        artist = plt.imshow(im, animated=True)
        artist_list.append([artist])
    return artist_list

# Render sizes
width, height = 500, 500
frames = 200
fps = 50
max_threads = 8

# Initialize pixel grid 
x = np.linspace(-2, 2, width)
y = np.linspace(-1, 1, height)
z = x[:, np.newaxis] + 1j * y[np.newaxis, :]
color = np.zeros_like(z, dtype=float)
pi_iter = np.linspace(0, 2 * pi, frames)
pool = Pool(max_threads)

# Calculate frames
import timeit
start_time = timeit.default_timer()

print("Rendering...")
ims = pool.starmap(compute_julia, zip(range(frames), pi_iter, repeat(z), repeat(color)))
ims = np.array(ims)

# Wait for all threads to finish
pool.close()
pool.join()

end_time = timeit.default_timer()
print("Rendering finished in {} seconds".format(round(end_time - start_time, 2)))

artist_list = create_artist_list(ims)

# Create animation loop
ani = ArtistAnimation(fig, artist_list, interval=1000/fps, blit=True)
plt.show()