from PIL import Image, ImageDraw, ImageColor, ImageCms
import numpy as np

# Load image
width, height = 800, 800
im = Image.new(mode='RGB', size=(width, height))
draw = ImageDraw.Draw(im)

# Split the image into 3x3 grid
grid = np.array([[[0, 0], [width // 3 + 1, 0], [(width // 3)*2 + 1, 0]],
                [[0, height // 3 + 1], [width // 3 + 1, height // 3 + 1], [(width // 3)*2 + 1, height // 3 + 1]],
                [[0, (width // 3)*2 + 1], [width // 3 + 1, (width // 3)*2 + 1], [(width // 3)*2 + 1, (width // 3)*2 + 1]]])

# Draw initial square
blue = ImageColor.getrgb('lightskyblue')
black = ImageColor.getrgb('black')
draw.rectangle([0, 0, width-1, height-1], fill=blue)
draw.rectangle([width // 3 + 1, height // 3 + 1, (width // 3)*2, (width // 3)*2], fill=black)

for i in range(5):
    copy = im.resize((width // 3, height // 3))
    draw.rectangle([0, 0, width-1, height-1], fill=blue)
    draw.rectangle([width // 3 + 1, height // 3 + 1, (width // 3)*2, (width // 3)*2], fill=black)
    im.paste(copy, box=(0, 0))
    im.paste(copy, box=(width // 3 + 1, 0))
    im.paste(copy, box=((width // 3)*2 + 1, 0))
    im.paste(copy, box=(0, height // 3 + 1))
    im.paste(copy, box=((width // 3)*2 + 1, height // 3 + 1))
    im.paste(copy, box=(0, (width // 3)*2 + 1))
    im.paste(copy, box=(width // 3 + 1, (width // 3)*2 + 1))
    im.paste(copy, box=((width // 3)*2 + 1, (width // 3)*2 + 1))

im.show()