import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from cmath import exp, pi

def compute_julia(frame, z, max_iter=25):
    c = 0.7885 * exp(1j * frame)
    color = np.zeros_like(z, dtype=float)

    for i in range(max_iter):
        color[z.real**2 + z.imag**2 < 4] = i
        z = np.where(z.real**2 + z.imag**2 < 4, z**2 + c, z)    
    
    ax.clear()
    ax.imshow(color)

# Render size
width, height = 250, 250

# Initial pixel grid
x = np.linspace(-2, 2, width)
y = np.linspace(-1, 1, height)
z = x[:, np.newaxis] + 1j * y[np.newaxis, :]

# Create figure and animation loop
fig, ax = plt.subplots()
ani = FuncAnimation(fig, compute_julia, fargs=(z,), frames=np.linspace(0, 2 * pi, 200), interval=20)
plt.show()