import numpy as np
import matplotlib.pyplot as plt

def compute_mandelbrot(max_iter, threshold, width, height):
    x = np.linspace(-1.5, 0.5, width)
    y = np.linspace(-1, 1, height)

    c = x[:, np.newaxis] + 1j * y[np.newaxis, :]

    z = c
    color = np.zeros_like(z, dtype=float)
    for i in range(max_iter):
        color[np.absolute(z) < threshold] = i
        z = np.where(np.absolute(z) > threshold, z, z**2 + c)

    return 1-color

if __name__ == "__main__":
    mandelbrot_set = compute_mandelbrot(50, 10, 1000, 1000)
    
    plt.imshow(mandelbrot_set.T)
    plt.show()