import numpy as np
import matplotlib.pyplot as plt

def compute_mandelbrot(max_iter, threshold, width, height):
    x = np.linspace(-1.5, 0.5, width)
    y = np.linspace(-1, 1, height)

    c = x[:, np.newaxis] + 1j * y[np.newaxis, :]
    z = c

    fig, ax = plt.subplots()
    ax = ax.imshow(np.random.random((width, height)))   # Only works with random

    for i in range(max_iter):
        z = z**2 + c
        mandelbrot_set = (abs(z) < threshold)
        ax.set_data(mandelbrot_set.T)
        plt.draw()
        plt.pause(1e-10)

if __name__ == "__main__":
    compute_mandelbrot(500, 2, 20, 20)
