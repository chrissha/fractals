import numpy as np
import matplotlib.pyplot as plt

def compute_julia(max_iter, threshold, width, height):
    x = np.linspace(-2, 2, width)
    y = np.linspace(-1, 1, height)

    z = x[:, np.newaxis] + 1j * y[np.newaxis, :]

    c = -0.835 - 0.2321j
    mask = np.ones_like(c, dtype=bool)
    color = np.zeros_like(z, dtype=float)
    for i in range(max_iter):
        mask = np.where(z.real**2 + z.imag**2 < 4, True, False)
        z = np.where(mask, z**2 + c, z)
        color[mask] = np.log(i + 1)

    return color

if __name__ == "__main__":
    julia_set = compute_julia(100, 10, 1500, 1000)
    
    plt.imshow(julia_set.T)
    plt.show()