import numpy as np
import matplotlib.pyplot as plt

width = 200
height = 200
minX=-1.5
maxX=0.5
minY=-1
maxY=1

initX = np.linspace(minX, maxX, width)
initY = np.linspace(minY, maxY, height)

def compute_mandelbrot(max_iter=500, threshold=10, minX=-1.5, maxX=0.5, minY=-1, maxY=1, width=200, height=200):
    # Computes the mandelbrot set
    x = np.linspace(minX, maxX, width)
    y = np.linspace(minY, maxY, height)
    c = x[:, np.newaxis] + 1j * y[np.newaxis, :]

    z = c
    color = np.zeros_like(z, dtype=float)
    for i in range(max_iter):   # TODO add mask to prevent overflow
        z = z**2 + c
        color[np.absolute(z) > threshold] = i

    return color

if __name__ == "__main__":
    mandelbrot_set = compute_mandelbrot()
    fig, ax = plt.subplots()
    im = ax.imshow(mandelbrot_set.T)

    def on_resize(ax):
        # Redraws the mandelbrot set after resize
        # Note: only updates when ylim is changed
        lims = ax.viewLim
        x0, y0, x1, y1 = int(round(lims.x0)), int(round(lims.y0)), int(round(lims.x1))-1, int(round(lims.y1))-1
        minX, maxX = initX[x0], initX[x1]
        minY, maxY = initY[y1], initY[y0]
        print("x0: {}, x1: {}, y0: {}, y1: {}, minX: {}, maxX: {}, miny: {}, maxY: {}, width: {}, height: {}".format(
            x0, x1, y0, y1, minX, maxX, minY, maxY, x1-x0, y0-y1
        ))
        subset = compute_mandelbrot(max_iter=500, minX=minX, maxX=maxX, minY=minY, maxY=maxY, width=width, height=height)
        im.set_data(subset.T)
        plt.draw()
    
    ax.callbacks.connect('ylim_changed', on_resize)
    plt.show()