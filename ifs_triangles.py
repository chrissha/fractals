from PIL import Image, ImageDraw, ImageColor, ImageCms

# Load image
width, height = 800, 600
im = Image.new(mode='RGB', size=(width, height))
draw = ImageDraw.Draw(im)

# Draw initial triangle
color = ImageColor.getrgb('lightskyblue')
draw.polygon([(10, 10), (width-10, 10), (width/2, height - 10)], fill=color)

# Ifs
for i in range(7):
    copy = im.resize((width // 2, height // 2))
    im.paste(copy, box=(0, 0))
    im.paste(copy, box=(width // 2, 0))
    im.paste(copy, box=(width // 2 - copy.width // 2, height // 2))

im.show()
